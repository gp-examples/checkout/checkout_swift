//
//  GreenPayProcess.swift
//  GreenPay Checkout Example
//
//  Created by Jose Saurez on 11/27/18.
//  Copyright © 2018 GreenPay. All rights reserved.
//

import Foundation
import CryptoSwift
import SwiftyRSA

class GreenPayProcess {
    
    static var GreenPaySecret = "";
    static var GreenPayMerchantId = "";
    static var GreenPayTerminalId = "";
    // La llave pública se separa en reglones y se le agrega \n string al terminar cada línea.
    static var GreenPayPublicKey = "";
    
    static var cardData : CardData = CardData();
    
    /**
     Nombre: startGreenPayProcess
     Descripción: Comienza el proceso de compra contra la plataforma Greenpay.
     */
    static func startGreenPayProcess(){
        
        var cardExpirationDate : CardExpirationDate = CardExpirationDate();
        cardExpirationDate.month = 9;
        cardExpirationDate.year = 21;
        
        var cardInfo : CardInfo = CardInfo ();
        cardInfo.cardHolder = "Jhon Doe";
        cardInfo.cardNumber = "4777777777777777";
        cardInfo.cvc = "123";
        cardInfo.cardExpirationDate = cardExpirationDate;
        cardInfo.nickname = "visa2449";
        
        cardData = CardData();
        cardData.cardInfo = cardInfo;
        
        // El orden para ejecutar la transacción:
        // 1. Crear una nueva orden.
        // 2. Realizar el proceso de Checkout de la orden.
        // 3. Verificar la firma de la respuesta de Greenpay para garantizar la integridad del mensaje.
        
        var orderRequestDataValue = OrderRequestData();
        orderRequestDataValue.secret = GreenPaySecret;
        orderRequestDataValue.merchantId = GreenPayMerchantId;
        orderRequestDataValue.terminal = GreenPayTerminalId;
        orderRequestDataValue.amount = 100;
        orderRequestDataValue.currency = "1";
        orderRequestDataValue.description = "desc";
        orderRequestDataValue.orderReference = "1";
        
        GreenPayProcess.postCreateOrder(orderRequestData: orderRequestDataValue, completion:
        {(success, orderResponseData)  -> Void in
                if success {
                    
                    let checkoutResponse : CheckoutResponseData = GreenPayProcess.postCheckoutOrder(cardData: cardData, orderResponseData: orderResponseData, completion: {(success, checkoutResponseData)  -> Void in
                        if success {
                            GreenPayProcess.verifyCheckoutResponse(checkoutResponseData: checkoutResponseData);
                        }
                    });
                    
                }
            }
        )
        
        
        // El orden para realizar la tokenización de una tarjeta:
        // 1. Crear una orden para tokenizar la tarjeta.
        // 2. Realizar el proceso de Checkout para la orden de tokenización.
        // 3. Verificar la firma de la respuesta de Greenpay para garantizar la integridad del mensaje.
        
        var tokenizeCardOrderRequestDataValue = TokenizeCardOrderRequestData();
        tokenizeCardOrderRequestDataValue.secret = GreenPaySecret;
        tokenizeCardOrderRequestDataValue.merchantId = GreenPayMerchantId;
        tokenizeCardOrderRequestDataValue.requestId = "1";
        
        GreenPayProcess.postTokenizeCardCreateOrder(orderRequestData: tokenizeCardOrderRequestDataValue, completion:
            {(success, orderResponseData)  -> Void in
                if success {
                    
                    let checkoutResponse : TokenizeCardCheckoutResponseData = GreenPayProcess.postTokenizeCardCheckoutOrder(cardData: cardData, orderResponseData: orderResponseData, completion: {(success, checkoutResponseData)  -> Void in
                        if success {
                            GreenPayProcess.verifyTokenizeCardCheckoutResponse(checkoutResponseData: checkoutResponseData, requestId: tokenizeCardOrderRequestDataValue.requestId);
                            
                            
                            // El orden para realizar la el pago de una orden con una tarjta tokenizada:
                            // 1. Obtener el token de la tarjeta previamente tokenizada.
                            // 2. Crear una orden para tokenizar la tarjeta.
                            // 3. Realizar el proceso de Checkout para la orden de tokenización.
                            // 4. Verificar la firma de la respuesta de GreenPay para garantizar la integridad del mensaje.
                            
                            let tokenizedCard : TokenizedCard = TokenizedCard();
                            tokenizedCard.token = checkoutResponseData.result.token;
                            
                            GreenPayProcess.postCreateOrder(orderRequestData: orderRequestDataValue, completion:
                                {(success, orderResponseData)  -> Void in
                                    if success {
                                        
                                        let checkoutResponse : CheckoutResponseData = GreenPayProcess.postCheckoutOrder(cardData: tokenizedCard, orderResponseData: orderResponseData, completion: {(success, checkoutResponseData)  -> Void in
                                            if success {
                                                GreenPayProcess.verifyCheckoutResponse(checkoutResponseData: checkoutResponseData);
                                            }
                                        });
                                        
                                    }
                                }
                            )
                            
                        }
                    });
                    
                }
        }
        )
    }
    
    /**
     Nombre: postCreateOrder
     Descripción: Realiza el proceso de creación de una nueva orden en la plataforma Greenpay.
     Retorna: El objeto OrderResponseData que contiene la respuesta a la creación de la orden.
     Parámetros: - OrderResponseData: Objeto OrderRequestData con los datos para crear una nueva orden.
     - completion - Bloque que contiene la función que se ejecuta al terminar de invoccar la función.
     */
    static func postCreateOrder(orderRequestData : OrderRequestData, completion: @escaping (Bool, OrderResponseData) -> Void ) -> OrderResponseData{
        var response1 : OrderResponseData = OrderResponseData();
        let url = URL(string: "https://sandbox-merchant.greenpay.me")!
        
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: url)
        
        let jsonEncoder = JSONEncoder()
        if let orderRequestDataBody = try? jsonEncoder.encode(orderRequestData){
                
            request.httpMethod = "POST"
            request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
            request.addValue("application/json", forHTTPHeaderField: "Content-type")
            request.httpBody = orderRequestDataBody
            print("*****Create Order Request : \(String(data: orderRequestDataBody, encoding: .utf8))")
            let task = session.dataTask(with: request as URLRequest) { (data, response, error) in
                guard let _: Data = data, let _: URLResponse = response, error == nil else {
                    print("*****error")
                    return
                }
                let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("*****Creat Order Response : \(dataString)")
                
                //JSONSerialization
                let jsonDecoder = JSONDecoder();
                let orderResponseData = try? jsonDecoder.decode(OrderResponseData.self, from: data!)
                response1 = orderResponseData!;
                
                completion(true, response1);
                
            }
            
            task.resume()
        }
        return response1;
    }
    
    /**
     Nombre: PostCheckout
     Descripción: Realiza el proceso de checkout en la plataforma Greenpay.
     Retorna: El objeto CheckoutResponseData que contiene la respuesta del proceso de Checkout.
     Parámetros: - cardData: Objeto que representa la tarjeta del cliente.
     - orderResponseData: Objeto orderResponseData con los datos para hacer checkout.
     - completion - Bloque que contiene la función que se ejecuta al terminar de invoccar la función.
     */
    static func postCheckoutOrder(cardData : CreditCard, orderResponseData : OrderResponseData, completion: @escaping (Bool, CheckoutResponseData) -> Void ) -> CheckoutResponseData{
        var response1 : CheckoutResponseData = CheckoutResponseData();
        let url = URL(string: "https://sandbox-checkout.greenpay.me")!
        
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: url)
        
        let checkoutRequestData : CheckoutRequestData = pack(cardData: cardData, orderResponseData: orderResponseData);
        
        let jsonEncoder = JSONEncoder()
        if let checkoutRequestDataBody = try? jsonEncoder.encode(checkoutRequestData){
            
            request.httpMethod = "POST"
            request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
            request.addValue("application/json", forHTTPHeaderField: "Content-type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue(orderResponseData.token, forHTTPHeaderField: "liszt-token")
            
            request.httpBody = checkoutRequestDataBody
            print("*****Checkout Order Request : \(String(data: checkoutRequestDataBody, encoding: .utf8))")
            let task = session.dataTask(with: request as URLRequest) { (data, response, error) in
                guard let _: Data = data, let _: URLResponse = response, error == nil else {
                    print("*****error")
                    return
                }
                let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("*****Checkout Response : \(dataString)")
                
                //JSONSerialization
                let jsonDecoder = JSONDecoder();
                let checkoutResponseData = try? jsonDecoder.decode(CheckoutResponseData.self, from: data!)
                response1 = checkoutResponseData!;
                
                completion(true, response1);
                
            }
            
            task.resume()
        }
        return response1;
    }
    
    /**
     Nombre: pack
     Descripción: Realiza los cálculos y procesamiento de datos para generar el objeto CheckoutRequestData, requerido para hacer el proceso de Checkout en Greenpay.
     Retorna: El objeto CheckoutRequestData que contiene todos los datos requeridos para realizar el proces de Checkout en Greenpay.
     Parámetros: - cardData: Objeto con los datos de la tarjeta con la que se desea pagar.
     - orderResponseData: Objeto que contiene la respuesta de la creación de la orden en Greenpay.
     */
    static func pack(cardData : CreditCard, orderResponseData : BaseOrderResponseData) -> CheckoutRequestData{
        
        // 1. Generar la llave para el cifrado AES en modo CTR
        let aesKey : AESKey = CipherUtils.generateAESKey();
        let keyBytes : [UInt8] = aesKey.getKeyValue();
        
        // 2. Generar el contador (Counter) para el cifrado AES en modo CTR
        let aesCounter : AESCounter = CipherUtils.generateAESCounter();
        let counterBytes : [UInt8] = aesCounter.getCounterValue();
        
        // 3. Convertir los datos del objeto que contiene la tarjeta a string y luego a bytes
        let jsonEncoder = JSONEncoder();
        let cardDataEncoded = try? jsonEncoder.encode(cardData);
        
        // 5.1 //  Utilizar AES en modo CTR para cifrar los datos de la tarjeta. Esta implementación utiliza la librería CrytoSwift para realizar el cifrado:
        //  - La llave AES generada representa en un arreglo de bytes.
        //  - El contador utilizado para el modo CTR representado en un arreglo de bytes.
        //  - No se debe usar padding.
        
        var aesHexString = AESUtils.cipherWithAESCTR(plainTextAsByteArray: [UInt8](cardDataEncoded!), key: keyBytes, counter: counterBytes)
        
        // 6.   Obtener el valor LK (Liszt Key) requerido para la trama de Checkout.
        // 6.1  Se deben guardar los valores de la llave (como un arreglo de bytes) y el counter (como un valor nunérico entero).
        // 6.2  Al serializado los dos valores se obtiene algo como:
        //                      "{\"k\":[98,191,20,0,70,53,133,100,8,131,110,91,79,186,218,166],\"s\":138}"
        //      En donde el valor "k" contiene la llave AES y el valor "s" contiene el counter usados para cifrar AES en modo CTR del paso 5.
        // 6.3  Utilizar el cifrado asimétrico RSA con la llave pública brindada por Greenpay para cifrar la trama de 6.2
        //      El resultado se encuentra representado en base64.
        var aesPair : AESPair = AESPair();
        aesPair.key = aesKey.key;
        aesPair.counter = aesCounter.counter;
        
        let aesPairEncoded = try? jsonEncoder.encode(aesPair);
        let aesPairStr = String(data: aesPairEncoded!, encoding: .utf8);
        
        var base64String = CipherUtils.cipherWithPublickKey(textToCipher: aesPairStr!, publicKeyContent: GreenPayProcess.GreenPayPublicKey);
        
        var checkoutRequestData : CheckoutRequestData = CheckoutRequestData();
        checkoutRequestData.session = orderResponseData.session;
        checkoutRequestData.lk = base64String;
        checkoutRequestData.ld = aesHexString;
        
        return checkoutRequestData;
    }

    /**
     Nombre: verifyCheckoutResponse
     Descripción: Valida que la respuesta obtenida por el proceso de Checkout de Greenpay fue generada en los servidores de Greenpay y no ha sido manipulada por terceras partes.
     Retorna: True si la firma de la respuesta del proceso de Checkout en Greenpay es correcta. Falso en caso contrario.
     Parámetros: - checkoutResponseData: Objeto con la respuesta del proceso de Checkout en Greenpay que contiene la firma a validar.
     */
    static func verifyCheckoutResponse(checkoutResponseData : CheckoutResponseData) -> Bool{
        
        let verifyValue = "status:" + String(checkoutResponseData.status) + ",orderId:" + String(checkoutResponseData.orderId);
        
        let isSuccessful = CipherUtils.verifySignature(valueToVerify: verifyValue, referenceValue: checkoutResponseData.signature, publicKeyStr: GreenPayProcess.GreenPayPublicKey);
        if (isSuccessful){
            print("SIGNTAURE IS VALID");
        }else{
            print("SIGNTAURE IS INVALID");
        }
        
        return isSuccessful;
    }
    
    /**
     Nombre: postTokenizeCardCreateOrder
     Descripción: Realiza el proceso de creación de una nueva orden para tokenizar tarjeta en la plataforma Greenpay.
     Retorna: El objeto TokenizeCardOrderResponseData que contiene la respuesta a la creación de la orden para tokenizar tarjeta.
     Parámetros: - orderRequestData: Objeto TokenizeCardOrderRequestData con los datos para crear una nueva orden de tokenización de tarjeta.
     - completion - Bloque que contiene la función que se ejecuta al terminar de invoccar la función.
     */
    static func postTokenizeCardCreateOrder(orderRequestData : TokenizeCardOrderRequestData, completion: @escaping (Bool, TokenizeCardOrderResponseData) -> Void ) -> TokenizeCardOrderResponseData{
        var response1 : TokenizeCardOrderResponseData = TokenizeCardOrderResponseData();
        let url = URL(string: "https://sandbox-merchant.GreenPay.me/tokenize")!
        
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: url)
        
        let jsonEncoder = JSONEncoder()
        if let orderRequestDataBody = try? jsonEncoder.encode(orderRequestData){
            
            request.httpMethod = "POST"
            request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
            request.addValue("application/json", forHTTPHeaderField: "Content-type")
            request.httpBody = orderRequestDataBody
            print("*****Tokenize Card Create Order Request : \(String(data: orderRequestDataBody, encoding: .utf8))")
            let task = session.dataTask(with: request as URLRequest) { (data, response, error) in
                guard let _: Data = data, let _: URLResponse = response, error == nil else {
                    print("*****error")
                    return
                }
                let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("*****Tokenize Card Create Order Response : \(dataString)")
                
                //JSONSerialization
                let jsonDecoder = JSONDecoder();
                let orderResponseData = try? jsonDecoder.decode(TokenizeCardOrderResponseData.self, from: data!)
                response1 = orderResponseData!;
                
                completion(true, response1);
                
            }
            
            task.resume()
        }
        return response1;
    }
    
    /**
     Nombre: postTokenizeCardCheckoutOrder
     Descripción: Realiza el proceso de checkout para tokenizar tarjetas en la plataforma Greenpay.
     Retorna: El objeto TokenizeCardCheckoutResponseData que contiene la respuesta del proceso de Checkout de tokenización de tarjeta.
     Parámetros: - cardData: Objeto que representa la tarjeta del cliente.
     - orderResponseData: Objeto TokenizeCardOrderResponseData con los datos para hacer checkout de tokenización de tarjeta.
     - completion - Bloque que contiene la función que se ejecuta al terminar de invoccar la función.
     */
    static func postTokenizeCardCheckoutOrder(cardData : CardData, orderResponseData : TokenizeCardOrderResponseData, completion: @escaping (Bool, TokenizeCardCheckoutResponseData) -> Void ) -> TokenizeCardCheckoutResponseData{
        var response1 : TokenizeCardCheckoutResponseData = TokenizeCardCheckoutResponseData();
        let url = URL(string: "https://sandbox-checkout.greenpay.me/tokenize")!
        
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: url)
        
        let checkoutRequestData : CheckoutRequestData = pack(cardData: cardData, orderResponseData: orderResponseData);
        
        let jsonEncoder = JSONEncoder()
        if let checkoutRequestDataBody = try? jsonEncoder.encode(checkoutRequestData){
            
            request.httpMethod = "POST"
            request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
            request.addValue("application/json", forHTTPHeaderField: "Content-type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue(orderResponseData.token, forHTTPHeaderField: "liszt-token")
            
            request.httpBody = checkoutRequestDataBody
            print("*****Tokenize Card Checkout Order Request : \(String(data: checkoutRequestDataBody, encoding: .utf8))")
            let task = session.dataTask(with: request as URLRequest) { (data, response, error) in
                guard let _: Data = data, let _: URLResponse = response, error == nil else {
                    print("*****error")
                    return
                }
                let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("*****Tokenize Card Checkout Response : \(dataString)")
                
                //JSONSerialization
                let jsonDecoder = JSONDecoder();
                let checkoutResponseData = try? jsonDecoder.decode(TokenizeCardCheckoutResponseData.self, from: data!)
                response1 = checkoutResponseData!;
                
                completion(true, response1);
                
            }
            
            task.resume()
        }
        return response1;
    }
    
    /**
     Nombre: verifyTokenizeCardCheckoutResponse
     Descripción: Valida que la respuesta obtenida por el proceso de Checkout de tokenización de tarjeta de Greenpay fue generada en los servidores de Greenpay y no ha sido manipulada por terceras partes.
     Retorna: True si la firma de la respuesta del proceso de Checkout de tokenización de tarjeta en Greenpay es correcta. Falso en caso contrario.
     Parámetros: - checkoutResponseData: Objeto con la respuesta del proceso de Checkout de tokenización de tarjeta en Greenpay que contiene la firma a validar.
     */
    static func verifyTokenizeCardCheckoutResponse(checkoutResponseData : TokenizeCardCheckoutResponseData, requestId : String) -> Bool{
        
        let verifyValue = "status:" + String(checkoutResponseData.status) + ",requestId:" + requestId;
        
        let isSuccessful = CipherUtils.verifySignature(valueToVerify: verifyValue, referenceValue: checkoutResponseData.signature, publicKeyStr: GreenPayProcess.GreenPayPublicKey);
        if (isSuccessful){
            print("TOKENIZE CARD SIGNTAURE IS VALID");
        }else{
            print("TOKENIZE CARD SIGNTAURE IS INVALID");
        }
        
        return isSuccessful;
    }
}
